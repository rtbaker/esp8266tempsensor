/*
 *  This sketch sends data via HTTP GET requests to data.sparkfun.com service.
 *
 *  You need to get streamId and privateKey at data.sparkfun.com and paste them
 *  below. Or just customize this script to talk to other HTTP servers.
 *
 */

#include <ESP8266WiFi.h>
#include <Base64.h>
#include "OneWire.h"
#include "DallasTemperature.h"
#include "credentials.h"

#if !defined(STREAMID) || !defined(PRIVATEKEY)
     #error "Must define STREAMID and PRIVATEKEY for sparkfun API."
#endif

#if !defined(SSID) || !defined(WIFIPASSWORD)
     #error "Must define SSID and WIFIPASSWORD."
#endif

const char* host = "data.sparkfun.com";

int setupDone = 0;

#define ONE_WIRE_BUS 2  // DS18B20 pin
OneWire *oneWire;
DallasTemperature *DS18B20;

void setup(){}

void mySetup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(SSID);
  
  WiFi.begin(SSID, WIFIPASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  oneWire = new OneWire(ONE_WIRE_BUS);
  DS18B20 = new DallasTemperature(oneWire);
}

float oldTemp = -1;

void loop() {
  if (!setupDone){
    mySetup();
    setupDone = 1;
  }
  
  delay(10000);

  float temp;
  
  do {
    DS18B20->requestTemperatures(); 
    temp = DS18B20->getTempCByIndex(0);
    Serial.print("Temperature: ");
    Serial.println(temp);
    delay(100);
  } while (temp == 85.0 || temp == (-127.0));

  if (oldTemp != temp){
    Serial.print("connecting to ");
    Serial.println(host);
  
    // Use WiFiClient class to create TCP connections
    WiFiClient client;
    const int httpPort = 80;
    if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      return;
    }
  
    // We now create a URI for the request
    String url = "/input/";
    url += STREAMID;
    url += "?private_key=";
    url += PRIVATEKEY;
    url += "&temp=";
    url += temp;
  
    Serial.print("Requesting URL: ");
    Serial.println(url);
  
    // This will send the request to the server
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return;
      }
    }
  
    // Read all the lines of the reply from server and print them to Serial
    while(client.available()){
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }
  
    Serial.println();
    Serial.println("closing connection");

    oldTemp = temp;
  }
}

