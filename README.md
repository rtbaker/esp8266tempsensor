# README #

ESP8266 Temperature probe.

### What is this repository for? ###

Very simple code for a temperature probe on an ESP8266 chip.

### How do I get set up? ###

* Get the Arduino dev enivroment: https://www.arduino.cc/en/Main/Software
* Add the ESP8266 Board manager URL to "Preferences" -> "Settings" -> "Additional board manager URL's". http://arduino.esp8266.com/stable/package_esp8266com_index.json
* Enable in "Tools" -> "Board" -> "Board manager"
